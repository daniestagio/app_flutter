import 'package:comunity/rede.dart';
import 'package:comunity/saldo.dart';
import 'package:comunity/suporte.dart';
import 'localizacao.dart';
import 'device.dart';
import 'configuracao.dart';

import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
        backgroundColor: Colors.purple[800],
        elevation: 0,
        leading:
            IconButton(icon: Icon(Icons.arrow_back_ios), onPressed: () => {}),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.search), onPressed: () => {})
        ],
      ),
      body: Column(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height / 2,
            decoration: BoxDecoration(
              color: Colors.purple[800],
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(30),
                bottomLeft: Radius.circular(30),
              ),
            ),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'Daniel Rodrigues',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 30,
                            ),
                          ),
                        ),
                        Row(
                          children: <Widget>[
                            Column(
                              children: <Widget>[
                                Text(
                                  'Ribeirão Preto - SP',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 18,
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              children: <Widget>[
                                Text(
                                  ' - Canada',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 18,
                                  ),
                                ),
                              ],
                            )
                          ],
                        )
                      ],
                    )
                  ],
                ),
              ],
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(40),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          IconButton(
                            icon: Icon(Icons.phone),
                            color: Colors.purple[800],
                            iconSize: 30,
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (BuildContext context) => Suporte(),
                                ),
                              );
                            },
                          ),
                          Text(
                            'Suporte',
                            style: TextStyle(
                              color: Colors.purple[800],
                              fontSize: 12,
                            ),
                          )
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          IconButton(
                            icon: Icon(Icons.attach_money),
                            color: Colors.purple[800],
                            iconSize: 30,
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (BuildContext context) => Saldo(),
                                ),
                              );
                            },
                          ),
                          Text(
                            'Saldo',
                            style: TextStyle(
                              color: Colors.purple[800],
                              fontSize: 12,
                            ),
                          )
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          IconButton(
                            icon: Icon(Icons.wifi),
                            color: Colors.purple[800],
                            iconSize: 30,
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (BuildContext context) => Rede(),
                                ),
                              );
                            },
                          ),
                          Text(
                            'Rede',
                            style: TextStyle(
                              color: Colors.purple[800],
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(40),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          IconButton(
                            icon: Icon(Icons.location_on),
                            color: Colors.purple[800],
                            iconSize: 30,
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      Localizacao(),
                                ),
                              );
                            },
                          ),
                          Text(
                            'Localização',
                            style: TextStyle(
                              color: Colors.purple[800],
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          IconButton(
                            icon: Icon(Icons.brightness_low),
                            color: Colors.purple[800],
                            iconSize: 30,
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      Configuracao(),
                                ),
                              );
                            },
                          ),
                          Text(
                            'Configuração',
                            style: TextStyle(
                              color: Colors.purple,
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          IconButton(
                            icon: Icon(Icons.laptop_mac),
                            color: Colors.purple[800],
                            iconSize: 30,
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (BuildContext context) => Device(),
                                ),
                              );
                            },
                          ),
                          Text(
                            'Device',
                            style: TextStyle(
                              color: Colors.purple[800],
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
