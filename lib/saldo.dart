import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Saldo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.purple[800],
      ),
      body: buildListView(),
    );
  }

  ListView buildListView() {
    return ListView(
      children: <Widget>[
        Container(
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: ListTile(
              title: Text('Mensalidade'),
              subtitle: Text('Em aberto'),
              leading: Icon(Icons.attach_money),
              onLongPress: () {},
            ),
          ),
        )
      ],
    );
  }
}
